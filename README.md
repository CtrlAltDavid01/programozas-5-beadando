# Raktár menedzsment rendszer

Egyszerű készletkezelési rendszer, a szerveroldal reaktív javában készült spring webflux segítségével, a template-ket thymeleaf kezeli, valamint a kliens oldalt Vue.js-el valósítottam meg. Az adatbázis PostgerSQL docker container-ben.

## Funkciók

- Admin felhasználó hozzáadhat felhasználókat, szerkesztheti őket, szerepköröket adhat neki, valamint törölheti őket.
- A raktári dolgozó hozzáadhat és törölhet termékeket, raktárba vehet szállítmányt, valamint kivehet belőle.
