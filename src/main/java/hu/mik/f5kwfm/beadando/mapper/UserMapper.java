package hu.mik.f5kwfm.beadando.mapper;

import hu.mik.f5kwfm.beadando.dto.user.SimpleUserDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserRequestDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserResponseDTO;
import hu.mik.f5kwfm.beadando.entity.User;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface UserMapper {
    UserResponseDTO mapUserResponseDTO(User user);

    SimpleUserDTO mapSimpleUserDTO(User user);

    User mapUser(UserRequestDTO userRequestDTO);
}
