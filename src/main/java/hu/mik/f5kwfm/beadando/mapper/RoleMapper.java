package hu.mik.f5kwfm.beadando.mapper;

import hu.mik.f5kwfm.beadando.dto.role.RoleDTO;
import hu.mik.f5kwfm.beadando.entity.Role;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface RoleMapper {
    RoleDTO mapRoleDTO(Role role);
}
