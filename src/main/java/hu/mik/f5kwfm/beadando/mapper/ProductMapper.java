package hu.mik.f5kwfm.beadando.mapper;

import hu.mik.f5kwfm.beadando.dto.product.ProductNameDTO;
import hu.mik.f5kwfm.beadando.dto.product.ProductRequestDTO;
import hu.mik.f5kwfm.beadando.dto.product.ProductDTO;
import hu.mik.f5kwfm.beadando.entity.Product;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ProductMapper {
    ProductDTO mapProductDTO(Product product);

    ProductNameDTO mapProductNameDTO(Product product);

    Product mapProduct(ProductRequestDTO product);
}
