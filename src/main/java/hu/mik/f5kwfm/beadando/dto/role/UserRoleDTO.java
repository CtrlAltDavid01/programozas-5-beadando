package hu.mik.f5kwfm.beadando.dto.role;

import lombok.Data;

@Data
public class UserRoleDTO {
    Long userId;
    String roleName;
}
