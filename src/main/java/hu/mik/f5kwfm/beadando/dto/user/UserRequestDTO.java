package hu.mik.f5kwfm.beadando.dto.user;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UserRequestDTO {
    private Long id;
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
