package hu.mik.f5kwfm.beadando.dto.role;

import lombok.Data;

@Data
public class RoleDTO {
    private Long id;
    private String name;
}
