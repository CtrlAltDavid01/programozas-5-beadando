package hu.mik.f5kwfm.beadando.dto.user;

import lombok.Data;

@Data
public class SimpleUserDTO {
    private Long id;
    private String username;
}
