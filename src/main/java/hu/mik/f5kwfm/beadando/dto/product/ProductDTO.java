package hu.mik.f5kwfm.beadando.dto.product;

import lombok.Data;

@Data
public class ProductDTO {
    private Long id;
    private String name;
    private int stock;
}
