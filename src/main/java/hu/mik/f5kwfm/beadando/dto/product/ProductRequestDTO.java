package hu.mik.f5kwfm.beadando.dto.product;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ProductRequestDTO {
    private Long id;
    @NotBlank
    private String name;
}
