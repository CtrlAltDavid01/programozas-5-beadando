package hu.mik.f5kwfm.beadando.dto.user;

import hu.mik.f5kwfm.beadando.dto.role.RoleDTO;
import hu.mik.f5kwfm.beadando.entity.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserResponseDTO {
    private Long id;
    private String username;
    private List<RoleDTO> roles;
}
