package hu.mik.f5kwfm.beadando.dto.product;

import lombok.Data;

@Data
public class ProductNameDTO {
    private Long id;
    private String name;
}
