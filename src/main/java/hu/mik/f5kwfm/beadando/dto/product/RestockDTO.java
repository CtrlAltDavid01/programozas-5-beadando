package hu.mik.f5kwfm.beadando.dto.product;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class RestockDTO {
    private Long productId;
    @Min(0)
    private int amount;
}
