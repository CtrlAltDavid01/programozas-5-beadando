package hu.mik.f5kwfm.beadando.service;

import hu.mik.f5kwfm.beadando.dto.product.*;
import hu.mik.f5kwfm.beadando.mapper.ProductMapper;
import hu.mik.f5kwfm.beadando.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public Flux<ProductNameDTO> findAll() {
        return this.productRepository.findAll().map(this.productMapper::mapProductNameDTO);
    }

    public Mono<ProductNameDTO> save(ProductRequestDTO product) {
        if(product.getId() == null) return this.productRepository.save(this.productMapper.mapProduct(product)).map(this.productMapper::mapProductNameDTO);

        return this.productRepository.findById(product.getId()).map(p -> {
            var v = this.productMapper.mapProduct(product);
            v.setStock(p.getStock());
            return v;
        }).flatMap(this.productRepository::save).map(this.productMapper::mapProductNameDTO);
    }

    public Mono<ProductDTO> findById(Long id) {
        return this.productRepository.findById(id).map(this.productMapper::mapProductDTO);
    }

    public Mono<Void> delete(Long id) {
        return this.productRepository.deleteById(id);
    }

    public Mono<Void> restock(RestockDTO restockDTO) {
        return this.productRepository.findById(restockDTO.getProductId()).map(p -> {
            p.setStock(p.getStock() + restockDTO.getAmount());
            return p;
        }).flatMap(this.productRepository::save).then();
    }

    public Mono<Object> sell(SellDTO sellDTO) {
        return this.productRepository.findById(sellDTO.getProductId()).map(p -> {
            p.setStock(p.getStock() - sellDTO.getAmount());
            return p;
        }).flatMap(p -> {
            if(p.getStock() < 0) return Mono.empty();
            else return this.productRepository.save(p);
        });
    }
}
