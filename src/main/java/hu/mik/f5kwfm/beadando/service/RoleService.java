package hu.mik.f5kwfm.beadando.service;

import hu.mik.f5kwfm.beadando.dto.role.RoleDTO;
import hu.mik.f5kwfm.beadando.entity.Role;
import hu.mik.f5kwfm.beadando.mapper.RoleMapper;
import hu.mik.f5kwfm.beadando.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    public Mono<Void> addRoleToUser(Long id, String role) {
        return this.roleRepository.findAllByUserId(id)
                .map(Role::getAuthority)
                .collectList()
                .map(l -> l.contains(role))
                .flatMap(b -> {
                    if (!b) return this.roleRepository.addRoleToUser(id, role);
                    else return Mono.empty();
                });
    }

    public Mono<Void> removeRoleFromUser(Long id, String role) {
        return this.roleRepository.removeRoleFromUser(id, role);
    }

    public Mono<Void> removeAllFromUser(Long id) {
        return this.roleRepository.removeAllFromUser(id);
    }
}
