package hu.mik.f5kwfm.beadando.service;

import hu.mik.f5kwfm.beadando.dto.user.SimpleUserDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserRequestDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserResponseDTO;
import hu.mik.f5kwfm.beadando.entity.Role;
import hu.mik.f5kwfm.beadando.entity.User;
import hu.mik.f5kwfm.beadando.mapper.UserMapper;
import hu.mik.f5kwfm.beadando.repository.RoleRepository;
import hu.mik.f5kwfm.beadando.repository.UserRepository;
import hu.mik.f5kwfm.beadando.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    private final PasswordEncoder passwordEncoder;

    public Flux<SimpleUserDTO> findAll() {
        return this.userRepository.findAll().map(this.userMapper::mapSimpleUserDTO);
    }

    public Mono<UserResponseDTO> save(UserRequestDTO userRequestDTO) {
        return Mono.just(userRequestDTO).map(this.userMapper::mapUser)
                .map(u -> {
                    u.setPassword(passwordEncoder.encode(u.getPassword()));
                    return u;
                }).flatMap(this.userRepository::save)
                .map(this.userMapper::mapUserResponseDTO);
    }

    public Mono<UserResponseDTO> findById(Long id) {
        return this.userRepository.findById(id)
                .flatMap(user -> this.roleRepository.findAllByUserId(user.getId())
                        .collectList()
                        .map(roles -> {
                            user.setRoles(roles);
                            return user;
                        })).map(this.userMapper::mapUserResponseDTO);
    }

    public Mono<Object> delete(Long id) {
        return this.hasRole(id, "ADMIN").flatMap(x -> x
                    ? Mono.empty()
                    : this.roleRepository.removeAllFromUser(id).then(this.userRepository.deleteById(id)).thenReturn(true));
    }

    public Mono<Boolean> hasRole(Long id, String role_name) {
        return this.roleRepository.findAllByUserId(id).map(Role::getAuthority).collectList().map(l -> l.contains(role_name));
    }
}
