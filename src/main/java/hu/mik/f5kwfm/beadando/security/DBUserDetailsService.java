package hu.mik.f5kwfm.beadando.security;

import hu.mik.f5kwfm.beadando.repository.RoleRepository;
import hu.mik.f5kwfm.beadando.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class DBUserDetailsService implements ReactiveUserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    public Mono<UserDetails> findByUsername(String username) {

        return this.userRepository.findByUsername(username)
                .switchIfEmpty(Mono.error(new UsernameNotFoundException(
                        "User with username " + username + "not found.")))
                .flatMap(user -> this.roleRepository.findAllByUserId(user.getId())
                        .collectList()
                        .map(roles -> {
                            user.setRoles(roles);
                            return new UserPrincipal(user);
                        }));
    }
}
