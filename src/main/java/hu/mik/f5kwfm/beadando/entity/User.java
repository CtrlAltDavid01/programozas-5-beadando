package hu.mik.f5kwfm.beadando.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@Data
@Table(value = "users")
public class User {

    @Id
    private Long id;
    private String username;
    private String password;
    @Transient
    private List<Role> roles;

}
