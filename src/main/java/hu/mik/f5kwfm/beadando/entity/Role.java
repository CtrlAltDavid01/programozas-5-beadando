package hu.mik.f5kwfm.beadando.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.security.core.GrantedAuthority;

@Data
@Builder
public class Role implements GrantedAuthority {

    @Id
    private Long id;
    @Column(value = "role_name")
    private String name;

    @Override
    public String getAuthority() {
        return this.name;
    }
}