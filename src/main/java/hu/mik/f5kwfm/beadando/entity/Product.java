package hu.mik.f5kwfm.beadando.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

@Data
public class Product {
    @Id
    private Long id;
    @Column(value = "product_name")
    private String name;
    private int stock;
}
