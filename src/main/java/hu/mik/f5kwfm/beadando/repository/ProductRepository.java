package hu.mik.f5kwfm.beadando.repository;

import hu.mik.f5kwfm.beadando.entity.Product;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ProductRepository extends ReactiveCrudRepository<Product, Long> {

}
