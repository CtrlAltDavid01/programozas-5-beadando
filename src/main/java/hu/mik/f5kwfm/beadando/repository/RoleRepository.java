package hu.mik.f5kwfm.beadando.repository;

import hu.mik.f5kwfm.beadando.entity.Role;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RoleRepository extends ReactiveCrudRepository<Role, Long> {

    @Query("SELECT r.* FROM role r JOIN user_role ur ON r.id = ur.role_id WHERE ur.user_id = :userId")
    Flux<Role> findAllByUserId(Long userId);

    @Modifying
    @Query("INSERT INTO user_role (user_id, role_id) VALUES (:userId, (SELECT id FROM role WHERE role_name = :roleName))")
    Mono<Void> addRoleToUser(Long userId, String roleName);

    @Modifying
    @Query("DELETE FROM user_role WHERE user_id = :userId AND role_id = (SELECT id FROM role WHERE role_name = :roleName)")
    Mono<Void> removeRoleFromUser(Long userId, String roleName);

    @Modifying
    @Query("DELETE FROM user_role WHERE user_id = :userId")
    Mono<Void> removeAllFromUser(Long userId);

    Mono<Role> findByName(String name);

}
