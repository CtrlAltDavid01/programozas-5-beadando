package hu.mik.f5kwfm.beadando.controller;

import hu.mik.f5kwfm.beadando.dto.product.*;
import hu.mik.f5kwfm.beadando.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('WAREHOUSE_WORKER')")
@RequestMapping(path = "api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    private final ProductService productService;

    @GetMapping(value = "products", consumes = MediaType.ALL_VALUE)
    public Flux<ProductNameDTO> listAll() {
        return this.productService.findAll();
    }

    @PutMapping("product")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ProductNameDTO> save(@Valid @RequestBody ProductRequestDTO product) {
        return this.productService.save(product);
    }

    @GetMapping(value = "product/{id}", consumes = MediaType.ALL_VALUE)
    public Mono<ProductDTO> findById(@PathVariable("id") Long id) {
        return this.productService.findById(id);
    }

    @DeleteMapping(value = "product/{id}", consumes = MediaType.ALL_VALUE)
    public Mono<Void> delete(@PathVariable("id") Long id) {
        return this.productService.delete(id);
    }

    @PostMapping("product/restock")
    public Mono<Void> restock(@Valid @RequestBody RestockDTO restockDTO) {
        return this.productService.restock(restockDTO);
    }

    @PostMapping("product/sell")
    public Mono<ResponseEntity<Object>> sell(@Valid @RequestBody SellDTO sellDTO) {
        return this.productService.sell(sellDTO).map(s -> ResponseEntity.status(HttpStatus.OK).body(null))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null)));
    }
}
