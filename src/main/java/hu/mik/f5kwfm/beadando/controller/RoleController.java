package hu.mik.f5kwfm.beadando.controller;

import hu.mik.f5kwfm.beadando.dto.role.UserRoleDTO;
import hu.mik.f5kwfm.beadando.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping(path = "api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {

    private final RoleService roleService;

    @PutMapping("role")
    public Mono<ResponseEntity<Void>> addRoleToUser(@RequestBody UserRoleDTO userRole) {
        if(userRole.getRoleName() == "ADMIN") return Mono.empty().map(s -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
        else return this.roleService.addRoleToUser(userRole.getUserId(), userRole.getRoleName()).map(s -> ResponseEntity.status(HttpStatus.CREATED).body(null));
    }

    @DeleteMapping("role")
    public Mono<ResponseEntity<Void>> removeRoleFromUser(@RequestBody UserRoleDTO userRole) {
        if(userRole.getRoleName() == "ADMIN") return Mono.empty().map(s -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
        else return this.roleService.removeRoleFromUser(userRole.getUserId(), userRole.getRoleName()).map(s -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(null));
    }
}
