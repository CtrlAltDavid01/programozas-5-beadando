package hu.mik.f5kwfm.beadando.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Mono;

@Controller
public class WebController {
    @GetMapping("/login")
    public Mono<String> login() {
        return Mono.just("login");
    }

    @GetMapping("/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Mono<String> users() {
        return Mono.just("users");
    }

    @GetMapping("/warehouse")
    @PreAuthorize("hasAuthority('WAREHOUSE_WORKER')")
    public Mono<String> warehouse() {
        return Mono.just("warehouse");
    }
}
