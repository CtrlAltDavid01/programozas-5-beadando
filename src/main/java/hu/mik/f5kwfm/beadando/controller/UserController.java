package hu.mik.f5kwfm.beadando.controller;

import hu.mik.f5kwfm.beadando.dto.user.SimpleUserDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserRequestDTO;
import hu.mik.f5kwfm.beadando.dto.user.UserResponseDTO;
import hu.mik.f5kwfm.beadando.entity.User;
import hu.mik.f5kwfm.beadando.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping(path = "api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @GetMapping(value = "users", consumes = MediaType.ALL_VALUE)
    public Flux<SimpleUserDTO> listAll() {
        return this.userService.findAll();
    }

    @PutMapping("user")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserResponseDTO> save(@Valid @RequestBody UserRequestDTO user) {
        return this.userService.save(user);
    }

    @GetMapping(value = "user/{id}", consumes = MediaType.ALL_VALUE)
    public Mono<UserResponseDTO> findById(@PathVariable("id") Long id) {
        return this.userService.findById(id);
    }

    @DeleteMapping(value = "user/{id}", consumes = MediaType.ALL_VALUE)
    public Mono<ResponseEntity<Object>> delete(@PathVariable("id") Long id) {
        return this.userService.delete(id).map(s -> ResponseEntity.status(HttpStatus.OK).body(null))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null)));
    }
}
