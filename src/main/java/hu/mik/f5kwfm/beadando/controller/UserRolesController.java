package hu.mik.f5kwfm.beadando.controller;

import hu.mik.f5kwfm.beadando.security.SecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRolesController {
    private final SecurityService securityService;

    @GetMapping("list-roles")
    public Mono<List<String>> listRoles() {
        return securityService.getRolesAsString();
    }
}
