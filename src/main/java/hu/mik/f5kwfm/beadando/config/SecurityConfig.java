package hu.mik.f5kwfm.beadando.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {
    private static final String[] AUTH_WHITELIST = {"/css/**", "/js/**", "/images/**", "/favicon.ico**", "/login", "/logout"};

    @Bean
    public SecurityWebFilterChain formLoginFilterChain(ServerHttpSecurity http) {
        return http.csrf(csrfSpec -> csrfSpec.csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse())).authorizeExchange(authorize -> {
            authorize.pathMatchers(AUTH_WHITELIST).permitAll();
            authorize.pathMatchers("/swagger-ui.html").hasAuthority("ADMIN");
            authorize.pathMatchers("/webjars/**").hasAuthority("ADMIN");
            authorize.anyExchange().authenticated();
        }).formLogin(form -> form.loginPage("/login")).logout().and().build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
