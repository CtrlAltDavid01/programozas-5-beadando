CREATE ROLE beadando_web WITH
    LOGIN
    SUPERUSER
    CREATEDB
    CREATEROLE
    INHERIT
    REPLICATION
    CONNECTION LIMIT -1
    PASSWORD '12345678';

CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL
);

CREATE TABLE role
(
    id        SERIAL PRIMARY KEY,
    role_name VARCHAR(100)
);

CREATE TABLE user_role
(
    id      SERIAL PRIMARY KEY,
    user_id SERIAL NOT NULL REFERENCES users (id),
    role_id SERIAL NOT NULL REFERENCES role (id)
);

CREATE TABLE product
(
    id              SERIAL PRIMARY KEY,
    product_name    VARCHAR(100) NOT NULL UNIQUE,
    stock           INT NOT NULL DEFAULT 0
);

INSERT INTO users (username, password)
VALUES ('admin', '$2a$12$yxkovcZ2JdGhxHOBa9U2BOqj3eTJMybgif0rggbHtyaOr4ZpX0yVS'); --Password: passwd
INSERT INTO users (username, password)
VALUES ('worker1', '$2a$12$yxkovcZ2JdGhxHOBa9U2BOqj3eTJMybgif0rggbHtyaOr4ZpX0yVS'); --Password: passwd

INSERT INTO role (role_name)
VALUES ('ADMIN');
INSERT INTO role (role_name)
VALUES ('WAREHOUSE_WORKER');

INSERT INTO user_role (user_id, role_id)
SELECT *
FROM (SELECT u.id FROM users u WHERE u.username = 'admin') a,
     (SELECT r.id FROM "role" r WHERE r.role_name = 'ADMIN') b;
INSERT INTO user_role (user_id, role_id)
SELECT *
FROM (SELECT u.id FROM users u WHERE u.username = 'admin') a,
     (SELECT r.id FROM "role" r WHERE r.role_name = 'WAREHOUSE_WORKER') b;
INSERT INTO user_role (user_id, role_id)
SELECT *
FROM (SELECT u.id FROM users u WHERE u.username = 'worker1') a,
     (SELECT r.id FROM "role" r WHERE r.role_name = 'WAREHOUSE_WORKER') b;

INSERT INTO product (product_name, stock)
VALUES ('cpu', 12);
INSERT INTO product (product_name, stock)
VALUES ('ram', 20);